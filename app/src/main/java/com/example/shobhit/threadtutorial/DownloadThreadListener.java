package com.example.shobhit.threadtutorial;

/**
 * Created by shobhit on 2/8/16.
 */
public interface DownloadThreadListener {
    public void updateStatus();
}
