package com.example.shobhit.threadtutorial;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import java.util.Random;

/**
 * Created by shobhit on 2/8/16.
 */
public class DownloadThread extends Thread{

    private Handler handler;
    private static final Random random = new Random();
    private static final String TAG = "DownloadThread";

    private DownloadThreadListener listener;

    private Integer audioRequestQueue = 0;
    private Integer audioDownloaded = 0;
    private Integer videoRequestQueue = 0;
    private Integer videoDownloaded = 0;

    private String randomString;

    DownloadThread(DownloadThreadListener listener){this.listener = listener;}

    @Override
    public void run() {
        //prepare looper for current thread which is detected automatically
        Looper.prepare();
        handler = new Handler();
        Looper.loop();
    }

    private Runnable audio = new Runnable() {
        @Override
        public void run() {
            try {
                Thread.sleep((random.nextInt(2) + 1) * 1000);
            }catch (Throwable t){
                Log.e(TAG, "Error downloading audio");
            }finally {
                synchronized (DownloadThread.this){
                    audioDownloaded++;
                    listener.updateStatus();//this is called by Download thread
                }
            }
        }
    };

    private Runnable video = new Runnable() {
        @Override
        public void run() {
            try {
                Thread.sleep((random.nextInt(5) + 1) * 1000);
            }catch (Throwable t){
                Log.e(TAG, "Error downloading video");
            }finally {
                synchronized (DownloadThread.this){
                    videoDownloaded++;
                    listener.updateStatus();//this is called by Download thread
                }
            }
        }
    };

    public synchronized void stopRequest(){
        //In this way, current looper will be quit once all the process queue gets empty
        handler.post(new Runnable() {
            @Override
            public void run() {
                Looper.myLooper().quit();
            }
        });
    }

    //this method can be called from any thread (like UI Thread)
    public synchronized void download(int id){
        // Check which radio button was clicked
        switch(id) {
            case R.id.video:
                //download video
                handler.post(video);//by posting to handler we make sure video is run in Download thread
                videoRequestQueue++;
                listener.updateStatus();//this is called by UI thread
                break;

            case R.id.audio:
                //download audio
                handler.post(audio);//by posting to handler we make sure audio is run in Download thread
                audioRequestQueue++;
                listener.updateStatus();//this is called by UI thread
                break;
        }
    }

    public synchronized void process(Runnable runnable){
        handler.post(runnable);//run in download thread
        listener.updateStatus();//called by UI thread
    }

    public Integer getAudioRequestQueue() {
        return audioRequestQueue;
    }

    public Integer getAudioDownloaded() {
        return audioDownloaded;
    }

    public Integer getVideoRequestQueue() {
        return videoRequestQueue;
    }

    public Integer getVideoDownloaded() {
        return videoDownloaded;
    }
}