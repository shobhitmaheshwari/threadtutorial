package com.example.shobhit.threadtutorial;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity implements DownloadThreadListener{

    Handler handler;
    DownloadThread downloadThread;
    private static final String TAG = "UIThread";

    private boolean radioButtonSelected = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Looper for ui thread is created internally. this handler will attach to it.
        handler = new Handler();
        downloadThread = new DownloadThread(this);
        downloadThread.start();

        RadioGroup radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                radioButtonSelected = true;
            }
        });

        TextView processResult = (TextView) findViewById(R.id.processResult);
        processResult.setText("Data is: " + data);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // stop the thread
        downloadThread.stopRequest();
    }

    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        if(radioButtonSelected){
            RadioGroup radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
            int selectedId = radioGroup.getCheckedRadioButtonId();
            downloadThread.download(selectedId);//notice that any processing done by Download thread is sequential
        }
    }

    //this method can be called from any thread (Like download thread or UI thread)
    @Override
    public void updateStatus() {
        //by posting to handler we make sure the following is run in UI thread
        handler.post(new Runnable() {
            @Override
            public void run() {
                TextView video = (TextView) findViewById(R.id.textViewVideoResult);
                video.setText("Video Status: " + downloadThread.getVideoDownloaded() +
                        "/" + downloadThread.getVideoRequestQueue());

                TextView audio = (TextView) findViewById(R.id.textViewAudioResult);
                audio.setText("Audio Status: " + downloadThread.getAudioDownloaded() +
                        "/" + downloadThread.getAudioRequestQueue());

                TextView processResult = (TextView) findViewById(R.id.processResult);
                processResult.setText("Data is: " + data);
            }
        });
    }

    private Integer data = 3;
    private static final Random random = new Random();

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            try {
                //we do heavy processing
                Thread.sleep((random.nextInt(data) + 1) * 1000);
                synchronized (MainActivity.this){
                    data++;
                }
            }catch (Throwable t){
                Log.e(TAG, "Error while processing");
            }finally {
                updateStatus();//called by Download thread
            }
        }
    };

    public void sendRunnable(View v){
        downloadThread.process(runnable);
    }
}